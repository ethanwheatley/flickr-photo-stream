# Web Development Task

You've been sent this task as we would love to get an idea of the way you work.

At Holiday Extras we use a variety of different technologies and
we don't expect you to be aware of all of them! We've designed this task with this in mind. It's worth noting that everything described below is a rough guideline of what we're looking for so don't feel you have to follow it exactly.

Your task is to consume the public [Flickr API](https://api.flickr.com/services/feeds/photos_public.gne?format=json) and display the results according to this basic wireframe:

![mockup](http://i.imgur.com/vyDVR1e.png)
_[Image source](http://i.imgur.com/vyDVR1e.png)_

We would expect this task to take a few hours, however there is no strict time limit and you won't be judged on how long it took you to complete. Please find a few pointers below:

* The website you create should function as a [single page app](http://en.wikipedia.org/wiki/Single-page_application).
* You must use HTML and CSS (using pre-compilers such as Sass or LESS is fine).
* Your app should support the major browsers (Chrome, Firefox, Safari, IE10+).
* We would encourage that you use a framework to speed up development time.
* If you do decide to use other Flickr API feeds to show us what you can do, be aware that some endpoints do require an API key. You can apply for your own [here](https://www.flickr.com/services/api/keys).

Although the main outcomes of the task are listed above, if you feel like you want to go that extra mile and show us what you're capable of, here is a list of potential enhancements that we have come up with:

* Think about how you might improve the speed of the page in terms of the images/content loading.
* Allow the page to have an infinite scroll (loading in more images as you scroll).
* A search functionality based on tags, or possibly the title of the photo.
* Alternatively if you can think of any other features that you feel would further enhance your app, then we'd love to see what you can come up with!

When you feel you've completed the task we would appreciate it if you could publish it online so we are in a position to be able to review it. We recommend that you upload your code to a Github/Bitbucket repository, however it's fine if you'd rather send it as an attachment in an email.

We look forward to seeing the results of your efforts.