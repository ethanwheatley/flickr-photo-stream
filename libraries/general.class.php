<?php
	
class general {
	
	/**
	* Auto compile less if changes have been made
	*/
	public static function autoCompileLess($sInputFile, $sOutputFile) {
		// load the cache
		$sCacheFile = $sInputFile . ".cache";

		if(file_exists($sCacheFile)) {
			$sCache	= unserialize(file_get_contents($sCacheFile));
		} else {
			$sCache	= $sInputFile;
		}

		$less 		= new \lessc;
		$aNewCache	= $less->cachedCompile($sCache);

		if(!is_array($sCache) || $aNewCache["updated"] > $sCache["updated"]) {
			file_put_contents($sCacheFile, serialize($aNewCache));
			file_put_contents($sOutputFile, $aNewCache['compiled']);
		}
	}
}