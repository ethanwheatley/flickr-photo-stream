<?php include( 'config.php' ); ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Flickr Photo Stream</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <script src="/js/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header>
	        <div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<h1>Flickr Photo Stream <i class="fa fa-cloud"></i></h1>
					</div>
				</div>
	        </div>
        </header>

		<section class="default">
	        <div class="container-fluid">
				<div class="row align-row" id="photo-holder" data-feed="http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=?">
			        <div class="col-sm-3 item template">
				        <div class="outer-holder">
					        <div class="image"><img src="#IMG#" /></div>
				        	<div class="author">
					        	<a href="#LINK#" target="_blank">#TITLE#</a> by <a href="https://www.flickr.com/photos/#AUTHOR_ID#" target="_blank">#AUTHOR#</a>
				        	</div>
				        	<div class="description">#DESCRIPTION#</div>
				        	<div class="tags" style="display: none;">Tags: <span>#TAGS#</span></div>
				        </div>
			        </div>
		        </div>
			</div>
		</section>
		
		<footer>
	        <div class="container-fluid">
		        <div class="row">
			        <div class="col-xs-12 text-center">Ethan Wheatley <?=date('Y'); ?></div>
		        </div>
			</div>
		</footer>

        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="/js/jquery-2.2.0.min.js"><\/script>')</script>
        <script src="/js/suitcase.js"></script>
        <script>
	        $(function(){
		        // Load photo stream on document ready
		        loadStream('photo-holder');
	        });
	    </script>
    </body>
</html>
<?php 
	
	// LESS to CSS compiler
	\general::autoCompileLess($_config['path'] . 'css/less/style.less', $_config['path'] . 'css/style.css');
	
?>