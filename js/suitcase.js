function loadStream(sContainer) {
	// Get the stream container into an object
	var oContainer	= $('#' + sContainer);
	
	// Load data attributes
	var oData		= oContainer.data();
	
	// Find the template and get the html object
	var oTemplate	= $('<div>').append(oContainer.find('.template').clone().removeClass('template')).remove();
	
	// Get the HTML from the template object
	var sTemplate 	= oTemplate.html();
	
	// Match marker replacements
	var aReplace	= sTemplate.match(/#(.*?)#/gi);
	
	$.ajax({
		type: 		'GET',
		dataType: 	'json',
	    url: 		oData.feed,
	    timeout: 	5000,
	    success: 	function(data, textStatus){
			$.each(data.items, function(i, item){
				var sHtml = sTemplate;			

				$.each(aReplace, function(x, sReplace){
					// Get out all variable tags to be used within the stream container
					var obj = sReplace.toLowerCase().replace(/#/g, '');
					var sOut;

					// The switch decides what type of content it is and how to treat it
					switch(obj) {
						case 'description':
							
							// Put feed content for description into an object
							var oHtml = $('<div>').append(item[obj]);
							
							var oParagraphs = oHtml.find('p');
							
							sOut = (oParagraphs.length > 2) ? oParagraphs.slice(2).html() : '';
												
						break;
						
						case 'img':
						
							// Put feed content for image into an object
							sOut = item.media.m;
					
						break;
						
						case 'author':
						
							// Get the outhor out from between the curvy brackets
							var sExp	= /\(([^)]+)\)/;
							var aMatch	= sExp.exec(item[obj]);

							// Get the first and only instance
							sOut = aMatch[1];
					
						break;
						
						case 'tags':

							// Get tag info from the feed data
							sOut  = item[obj];
							var aTags = sOut.split(' ');
							
							if(aTags.length > 1) {
								var oHtml = $('<div>').append(sHtml);
								
								// Locate the tags area and remove the display none to show that element
								oHtml.find('.tags').css('display', 'block');
								
								sHtml = oHtml.html();
								sOut  = aTags.join(', '); 
							}
					
						break;
						
						default:
							
							// If not already defined just get the content straight
							sOut = item[obj];
					
						break;
					}
					
					// Do the replacement in the copy of the template HTML
					sHtml = sHtml.replace(sReplace, sOut);
				});
				
				oContainer.append(sHtml);
			})
	    },
		error: function(xhr, textStatus, errorThrown){
			// If there is a failure with the feed then show the user some kind of feedback
			oContainer.html('<div class="col-sm-12 text-center"><div class="alert" role="alert"><i class="fa fa-frown-o"></i>Sorry, we are unable to load the photo stream at the moment</div></div>');
		}
	});
}